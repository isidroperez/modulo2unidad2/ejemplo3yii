<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenistas".
 *
 * @property int $id
 * @property string $nombre
 * @property string $correo
 * @property int $activo
 * @property string $fechaBaja
 * @property string $fechaNacimiento
 * @property int $altura
 * @property int $peso
 * @property int $idnacion
 *
 * @property Naciones $nacion
 */
class tenistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo', 'altura', 'peso', 'idnacion'], 'integer'],
            [['fechaBaja', 'fechaNacimiento'], 'safe'],
            [['nombre'], 'string', 'max' => 20],
			[['correo'],'email'],
            [['idnacion'], 'exist', 'skipOnError' => true, 'targetClass' => Naciones::className(), 'targetAttribute' => ['idnacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'correo' => 'Correo',
            'activo' => 'Activo',
            'fechaBaja' => 'Fecha de Baja',
            'fechaNacimiento' => 'Fecha de Nacimiento',
            'altura' => 'Altura(cm)',
            'peso' => 'Peso()kg',
            'idnacion' => 'Nación',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNacion()
    {
        return $this->hasOne(Naciones::className(), ['id' => 'idnacion']);
    }
}
